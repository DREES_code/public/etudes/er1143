# ER1143

Ce dossier fournit le programme permettant de produire les illustrations de l'Études et Résultats n° 1143 de la DREES : "Les personnes ayant des incapacités quittent le marché du travail plus jeunes mais liquident leur retraite plus tard".
Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/les-personnes-ayant-des-incapacites-quittent-le-marche-du-travail-plus-jeunes

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : enquêtes Emploi (INSEE) ; traitements : DREES.

L'approche s'appuie sur deux indicateurs principaux :
- l'indicateur de restrictions d'activité générale (GALI), pour le repérage des personnes handicapées
- l'âge conjoncturel de départ à la retraite, pour le calcul des indicateurs de retraite.

Le programme a été exécuté pour la dernière fois le 11/10/2020 avec le logiciel R version 3.5.1.

